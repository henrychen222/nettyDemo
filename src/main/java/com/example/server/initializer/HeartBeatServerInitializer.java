/**
 * 11/06/23 afternoon
 * 心跳检测机制
 */
package com.example.server.initializer;

import com.example.server.handler.HeartBeatServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.timeout.IdleStateHandler;

public class HeartBeatServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel sc) {
        ChannelPipeline pipeline = sc.pipeline();
        pipeline.addLast(new ObjectEncoder());

        // IdleStateHandler: 心跳机制处理器，主要用来检测远端是否读写超时, 如果超时则将超时事件传入userEventTriggered(ctx, )

        pipeline.addLast(new IdleStateHandler(3, 5, 7));

        pipeline.addLast(new HeartBeatServerHandler());
    }
}

