/**
 * 11/07/23 night
 * 基于CS架构的WebSocket通信
 */
package com.example.server.initializer;

import com.example.server.handler.WebSocketServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;

public class WebSocketServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel sc) {
        ChannelPipeline pipeline = sc.pipeline();
        pipeline.addLast(new HttpServerCodec());

        /**
         HttpObjectAggregator: 把多个HttpMessage组装成一个完整的Http请求(FullHttpRequest)或者响应(FullHttpResponse)
         如果自定义处理器是Inbound, 则表示请求
         如果是OutBound,就表示响应
         */
        pipeline.addLast(new HttpObjectAggregator(4096));

        // 处理websocket的netty处理器，可以通过构造方法绑定websocket的服务端地址
        pipeline.addLast(new WebSocketServerProtocolHandler("/myWebSocket"));

        // 自定义处理器
        pipeline.addLast(new WebSocketServerHandler());
    }
}

