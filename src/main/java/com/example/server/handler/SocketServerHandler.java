/**
 * 10/14/23 afternoon
 * 使用Netty开发点对点通信与聊天室功能 I
 */
package com.example.server.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Scanner;

public class SocketServerHandler extends SimpleChannelInboundHandler<String> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String receiveMsg) {
        System.out.println("[服务端]接收的请求来自: " + ctx.channel().remoteAddress() + ", 消息内容: " + receiveMsg);
        System.out.println("请向[客户端]端发送一条消息---");
        String sendMsg = new Scanner(System.in).nextLine();
        ctx.channel().writeAndFlush(sendMsg);
    }
}

