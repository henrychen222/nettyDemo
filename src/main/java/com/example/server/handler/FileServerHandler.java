/**
 * 11/04/23 afternoon
 * 远程传输文件
 */
package com.example.server.handler;

import com.example.server.model.SendFile;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.io.File;
import java.io.RandomAccessFile;

public class FileServerHandler extends SimpleChannelInboundHandler {

    private int readLength;
    private int start = 0;
    private String receivePath = "e:/upload"; // 接收文件的路径

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof SendFile) {
            SendFile sendFile = (SendFile) msg;
            byte[] bytes = sendFile.getBytes();
            readLength = sendFile.getEnd();
            String fileName = sendFile.getFileName();
            String path = receivePath + File.separator + fileName;
            File file = new File(path);
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(start);
            randomAccessFile.write(bytes);
            start += readLength;
            if (readLength > 0) {
                ctx.writeAndFlush(start);
                randomAccessFile.close();
            } else {
                ctx.flush().close();
            }
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        ctx.flush().close();
    }
}

