/**
 * 11/07/23 night
 * 基于CS架构的WebSocket通信
 */
package com.example.server.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

// 泛型TextWebSocketFrame: WebSocket处理的处理文本类型
public class WebSocketServerHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame frame) throws Exception {
        System.out.println("Server收到消息: " + frame.text());
        ctx.channel().writeAndFlush(new TextWebSocketFrame("hello client...")); // 向WebSocket客户端发送数据
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        System.out.println("客户端加入: id=" + ctx.channel().id());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        System.out.println("客户端离开: id=" + ctx.channel().id());
    }
}

