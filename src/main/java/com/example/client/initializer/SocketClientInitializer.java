/**
 * 11/04/23 afternoon
 * 使用Netty开发点对点通信与聊天室功能 I
 */
package com.example.client.initializer;

import com.example.client.handler.SocketClientHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

public class SocketClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) {
        ChannelPipeline channelPipeline = socketChannel.pipeline();
        channelPipeline.addLast("LengthFieldBasedFrameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 8, 0, 8));
        channelPipeline.addLast("LengthFieldPrepender", new LengthFieldPrepender(8));
        channelPipeline.addLast("StringDecoder", new StringDecoder(CharsetUtil.UTF_8));
        channelPipeline.addLast("StringEncoder", new StringEncoder(CharsetUtil.UTF_8));

        channelPipeline.addLast("MyNettyClientHandler", new SocketClientHandler());
    }
}
