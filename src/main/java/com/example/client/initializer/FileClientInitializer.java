/**
 * 11/06/23 afternoon
 * 远程传输文件
 */
package com.example.client.initializer;

import com.example.client.handler.FileClientHandler;
import com.example.server.model.SendFile;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

public class FileClientInitializer extends ChannelInitializer<SocketChannel> {
    private SendFile sendFile;

    public FileClientInitializer(SendFile fileUploadFile) {
        this.sendFile = fileUploadFile;
    }

    @Override
    protected void initChannel(SocketChannel sc) {
        ChannelPipeline pipeline = sc.pipeline();
        pipeline.addLast(new ObjectEncoder());
        pipeline.addLast(new ObjectDecoder(ClassResolvers.weakCachingConcurrentResolver(null)));

        pipeline.addLast(new FileClientHandler(sendFile));
    }
}
