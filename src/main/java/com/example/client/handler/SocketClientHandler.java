/**
 * 11/04/23 afternoon
 * 使用Netty开发点对点通信与聊天室功能 I
 */
package com.example.client.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Scanner;

public class SocketClientHandler extends SimpleChannelInboundHandler<String> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String receiveMsg) {
        System.out.println("[客户端]接收的请求来自: " + ctx.channel().remoteAddress() + ", 消息内容: " + receiveMsg);
        System.out.println("请向[服务端]发送一条消息---");
        String sendMsg = new Scanner(System.in).nextLine();
        ctx.channel().writeAndFlush(sendMsg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ctx.writeAndFlush("打破僵局的第一条消息...");
    }
}
