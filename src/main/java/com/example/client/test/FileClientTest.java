/**
 * 11/06/23 afternoon
 * 远程传输文件
 */
package com.example.client.test;

import com.example.client.initializer.FileClientInitializer;
import com.example.server.model.SendFile;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.apache.commons.logging.Log;

import java.io.File;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;

public class FileClientTest {

    public static void connect(int port, String host, final SendFile fileUploadFile) throws Exception {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            ChannelFuture channelFuture = bootstrap.group(eventLoopGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new FileClientInitializer(fileUploadFile))
                    .connect(host, port).sync();
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            eventLoopGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) {
        int port = 8888;
        if (args != null && args.length > 0) {
            try {
                port = Integer.valueOf(args[0]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        try {
            SendFile sendFile = new SendFile();
//            File file = new File("d:/JDK_API.CHM");
            File file = new File("/Users/weichen/Documents/Self-Study/Java Backend Demo/Netty/src/main/resources/upload_test.txt");
            System.out.println("start uploading file ........ upload_test.txt");
            String fileName = file.getName();
            sendFile.setFile(file);
            sendFile.setFileName(fileName);
            sendFile.setStart(0);
            connect(port, "127.0.0.1", sendFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
